# 2022 G. Brandt <gbrandt@cern.ch>

FROM cc7-sysd

SHELL ["/bin/bash", "--login", "-c"]


# itk-demo-configdb
RUN demi self update
RUN demi services init
# RUN demi services info

RUN demi mariadb install


RUN demi -v services add configdb
RUN demi -v services install configdb

# itk-demo-daqapi
RUN demi services add daqapi
RUN demi services install daqapi

# itk-demo-resultviewer
RUN demi services add resultviewer
RUN demi services install resultviewer
RUN demi jsroot install
# RUN demi services add daqapi

# ADD cmd.sh /usr/local/bin/
# RUN chmod +x /usr/local/bin/cmd.sh

# ADD run_demi.sh /root
# RUN chmod +x /root/run_demi.sh
# CMD ["/usr/local/bin/cmd.sh"]
