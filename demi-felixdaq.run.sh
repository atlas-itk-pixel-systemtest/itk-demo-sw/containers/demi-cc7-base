#!/bin/bash
# shellcheck source=/dev/null

# to do: run as login shell
source /root/.bashrc

FULLNAME=$(getent passwd "$USER" | cut -d ':' -f 5 | cut -d ',' -f 1)

# to do: use demi git
git config --global user.email "${USER}@cern.ch"
git config --global user.name "${FULLNAME}"

source /root/demi/setup.sh
demi self update
cd /root/itk-demo-sw || exit

run_confidb() {
    demi mariadb start
    systemctl status mariadb
    demi mariadb setup
    demi configdb copyinst mariadb
    demi configdb upgrade
    demi services start configdb
    demi configdb fill -s localhost:5000 -y
    systemctl status configdb
}

run_resultviewer() {
    demi services start resultviewer
    systemctl status resultviewer
}

run_daqapi_felixdaq() {
    # cd /root/itk-demo-sw/itk-demo-daqapi || exit
    demi -v felixdaq runall
    demi services start daqapi
    systemctl status daqapi
}

run_nginx() {
    demi nginx configure
    demi nginx start
    systemctl status nginx
}

run_confidb
run_resultviewer
run_nginx
run_daqapi_felixdaq


