#!/bin/bash
# shellcheck source=/dev/null

# to do: run as login shell
source /root/.bashrc

FULLNAME=$(getent passwd "$USER" | cut -d ':' -f 5 | cut -d ',' -f 1)

# to do: use demi git
git config --global user.email "${USER}@cern.ch"
git config --global user.name "${FULLNAME}"

source /root/demi/setup.sh
demi self update
cd /root/itk-demo-sw || exit

run_confidb() {
    demi mariadb start
    demi mariadb setup

    # cd /root/itk-demo-sw/itk-demo-configdb || exit
    # demi python info
    # cd /root/itk-demo-sw || exit

    demi configdb copyinst mariadb
    demi configdb upgrade
    demi services start configdb
    # demi configdb fill -s localhost:5000
    demi configdb fill -s localhost:5000 -y

    systemctl status configdb
}

run_resultviewer() {
    demi services start resultviewer
    systemctl status resultviewer
}

run_daqapi_felixdaq() {
    cd /root/itk-demo-sw/itk-demo-daqapi || exit
    #poetry add pybind11
    demi cvmfs install
    demi cvmfs info
    # pip install pybind11
    demi -v felixdaq configure
    demi services start daqapi
}

run_daqapi_yarr() {
    demi yarr info
    demi services start daqapi
    systemctl status daqapi
}

run_nginx() {
    demi nginx configure
    demi nginx start
    systemctl status nginx
}

run_confidb
run_daqapi_yarr
run_resultviewer
run_nginx


