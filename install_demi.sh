#!/bin/bash
# shellcheck source=/dev/null

systemctl reset-failed

cd || exit
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi.git
source ./demi/setup.sh

demi info

echo "cd $HOME/itk-demo-sw" >> .bashrc && \
echo "source $HOME/demi/setup.sh" >> .bashrc

mkdir itk-demo-sw
mv .gitlab-token itk-demo-sw
cd itk-demo-sw || exit

demi services init

demi -v cvmfs install
#demi cvmfs info

# demi python init

# demi -v services add itk-demo-optoboard
# demi -v services install

# cd itk-demo-optoboard || exit
# poetry shell
# flask run


