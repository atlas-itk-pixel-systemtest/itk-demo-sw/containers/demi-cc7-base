
FROM cern/cc7-base:latest

# use bash login shell
SHELL ["/bin/bash", "--login", "-c"]
RUN cd && touch .bashrc

# add CERN CentOS yum repo
# ADD http://linux.web.cern.ch/linux/centos7/CentOS-CERN.repo /etc/yum.repos.d/CentOS-CERN.repo
# ADD http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/RPM-GPG-KEY-cern /tmp/RPM-GPG-KEY-cern
# RUN /usr/bin/rpm --import /tmp/RPM-GPG-KEY-cern && \
#     /bin/rm /tmp/RPM-GPG-KEY-cern

# RUN yum groups mark convert
# RUN yum groupinstall --skip-broken -y 'Software Development Workstation (CERN Recommended Setup)'
# RUN yum --enablerepo=*-testing clean all

RUN yum update -y && \
    yum clean all && \
	  /bin/rm -rf /var/cache/yum

RUN yum install -y curl git sudo patch make which

# the steps below are for temporary speedup and should really be handled by demi

# pre-install python

RUN yum install -y gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel
RUN curl https://pyenv.run | bash
RUN cd && echo 'export PATH="$HOME/.pyenv/bin:$PATH"' >> .bashrc && \
    echo 'eval "$(pyenv init --path)"' >> .bashrc && \
    echo 'eval "$(pyenv virtualenv-init -)"' >> .bashrc
RUN pyenv install 3.8.10

# preinstall NVM

RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
RUN nvm install 16.13.2

# preinstall demi

RUN yum install -y jq
RUN cd && \
    git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi.git

RUN mkdir itk-demo-sw
ADD .gitlab-token /root/itk-demo-sw/

RUN cd && \
    echo "cd $HOME/itk-demo-sw" >> .bashrc && \
    echo "source $HOME/demi/setup.sh" >> .bashrc

RUN demi services init

#cd && source demi/setup.sh
# demi cvmfs install
# demi cvmfs info

RUN demi nodejs setup
RUN demi nodejs info

RUN demi python setup
RUN demi python install
RUN demi python info

RUN demi services add itk-demo-optoboard
RUN demi services install


# ENTRYPOINT [ "executable" ]

## EOF

