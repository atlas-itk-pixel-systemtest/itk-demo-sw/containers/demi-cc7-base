#!/bin/bash
# shellcheck source=/dev/null

error_exit()
{
  echo "Error $1"
  exit 1
}


_cpu=$(uname -m)

# _base_image="cern/cc7-base:latest" \
#    && [ "${_cpu}" == "arm64" ] || [ "$1" == "--do-not-use-cc7" ] \
#    && _base_image="centos:centos7"

_base_image="centos:centos7"

echo "Using base image: ${_base_image}"
# build it

docker build --rm -f cc7-sysd.Dockerfile -t cc7-sysd \
  --build-arg base_image=${_base_image} . --progress plain \
  || error_exit "Building systemd image failed!"
echo "Built Centos-7 systemd image"

docker build --rm -f demi-basic.Dockerfile -t demi-basic . --progress plain \
  || error_exit "Building basic demi chain image failed!"
echo "Built basic demi chain image"

docker build --rm -f demi-yarr.Dockerfile -t demi-yarr . --progress plain \
   || error_exit "Building demi YARR image failed"
echo "Built demi YARR image"

# docker run --name demi-yarr --rm --privileged -ti -v /run -v /sys/fs/cgroup:/sys/fs/cgroup -p 80:80 demi-yarr

docker build --rm -f demi-felixdaq.Dockerfile -t demi-felixdaq . --progress plain \
   || error_exit "Building demi felixdaq image failed"
echo "Built demi felixdaq image"

# docker run --name demi-felixdaq --rm --privileged -v /run -v /sys/fs/cgroup:/sys/fs/cgroup -v /cvmfs:/cvmfs:shared -p 80:80 demi-felixdaq


