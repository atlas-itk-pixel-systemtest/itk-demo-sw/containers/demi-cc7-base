# 2022 G. Brandt <gbrandt@cern.ch>

FROM demi-basic

SHELL ["/bin/bash", "--login", "-c"]

# pyyarr
RUN yum -y install devtoolset-10
RUN yum -y install cmake3

RUN demi yarr download
RUN cd /root/itk-demo-sw/YARR_PYTHON && demi python setup
RUN scl enable devtoolset-10 'demi yarr configure'
RUN scl enable devtoolset-10 'demi yarr build'
RUN scl enable devtoolset-10 'demi yarr install'

ADD cmd.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/cmd.sh

ADD demi-yarr.run.sh /root
RUN chmod +x /root/demi-yarr.run.sh

RUN echo "root:root" | chpasswd 
CMD ["/usr/local/bin/cmd.sh"]
