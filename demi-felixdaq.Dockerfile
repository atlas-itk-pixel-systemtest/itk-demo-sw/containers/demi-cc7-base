# 2022 G. Brandt <gbrandt@cern.ch>

FROM demi-basic

SHELL ["/bin/bash", "--login", "-c"]

# felixdaq

RUN yum -y install which redhat-lsb-core

ADD cmd.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/cmd.sh

ADD demi-felixdaq.run.sh /root
RUN chmod +x /root/demi-felixdaq.run.sh

RUN echo "root:root" | chpasswd 
CMD ["/usr/local/bin/cmd.sh"]
