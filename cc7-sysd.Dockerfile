# This shows systemd services running in a centos7 container.
# There have been lots of problems and workarounds for this, see:
# https://hub.docker.com/_/centos/
# https://github.com/docker/docker/issues/7459
# https://github.com/docker-library/docs/tree/master/centos#systemd-integration
# https://forums.docker.com/t/any-simple-and-safe-way-to-start-services-on-centos7-systemd/5695/8
ARG base_image
FROM ${base_image}
ENV container=docker

# use bash login shell
SHELL ["/bin/bash", "--login", "-c"]
RUN cd && touch .bashrc
RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;
RUN rm -f /etc/yum.repos.d/cern /etc/yum.repos.d/*source*
RUN yum -v clean all && yum makecache
RUN yum update -y && yum -y upgrade
RUN yum install -y systemd && yum clean all
RUN yum install -y epel-release # for nginx

RUN yum install -y centos-release-scl

RUN yum install -y git jq sudo patch make

RUN yum install -y centos-indexhtml nginx

RUN mkdir -p /root/itk-demo-sw
ADD .gitlab-token /root/itk-demo-sw/

RUN cd && git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi.git
RUN echo ". $HOME/demi/setup.sh" >> /root/.bashrc
RUN echo "cd $HOME/itk-demo-sw" >> /root/.bashrc

# RUN demi -v services init

# don't preinstall explicitly, test correct dependency resolution by demi
# RUN demi -v python init
# RUN demi -v nodejs init

# CERN-VM FS should run on host, it's not made for containers
# RUN yum install -y https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
# RUN demi -v cvmfs install

# RUN demi -v nginx install
# RUN demi -v nginx configure
# TO DO:  move to demi nginx configure
# RUN chmod 755 /root
# RUN chmod 755 /root/itk-demo-sw
# RUN chmod 755 /root/itk-demo-sw/www

# RUN BROWSE_MODE="no" /usr/sbin/automount -t 0 -f /etc/auto.master
# RUN cvmfs_config chksetup
# RUN cvmfs_config probe
# RUN cvmfs_config status
# RUN cvmfs_config stat -v

# Without this, init won't start the enabled services and exec'ing and starting
# them reports "Failed to get D-Bus connection: Operation not permitted".
# (n.b. still doesn't work, need to start from login shell...)
VOLUME /run /tmp
VOLUME /sys/fs/cgroup

EXPOSE 80

