#!/bin/bash

# delayed execution of install script
setsid /root/run_demi.sh &
# > run_demi.log &


# systemd has to be first process
exec /usr/sbin/init
